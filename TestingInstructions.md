# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# 
#	Group 12
#
#	Team Members: Sebastian Crites, Kevin Reyes, Richard Pham, Rik Mantel
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

#
#	TESTING INSTRUCTIONS
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

#	So you're ready to test our code? We made it simple for you to do so. 
#	
#	Step 1: Ensure you have the following classes downloaded:
#		Game.java, Event.java, EventTest.java, LinearStory.java, LinearStoryTest.java, MasterTest.java, Story.java, StoryReader.java, StoryReaderTest.java, and StoryTest.java
#		(essentially all .java classes we have available in the repository)
#		!! Additionally, ensure the compiled bitcode of all the individual test classes are properly within the test folder.
#
#	Step 2: Ensure you have the the document 'Narration.txt' in the same folder. The tests will use this document!
#	
#	Step 3: Compile MasterTest.java from the command line using:
#				javac -d . MasterTest.java
#			Execute MasterTest using:
#				java story.MasterTest
#			(this is required because of the test package)

#
#	Expected Output:
#		"TESTING CLASSES...
#				All tests were completed and successful! =D"
#
#	This test will run all of the test classes and ensure that the code is working properly. 