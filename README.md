# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# 
#	Group 12
#
#	Team Members: Sebastian Crites, Kevin Reyes, Richard Pham, Rik Mantel
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


#
#	INFORMATION BELOW
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

#	- Please Note: We have created a new repository for the updated version of this project.
#	- Please ensure you're accessing the correct information from within the correct repository. 

#	- DesignDocument.pdf: Code Repository (StoryCraft) on BitBucket. 
#	- Code Repository: Bit Bucket (https://bitbucket.org/cpscgroup12/storycraft)

# !! How to Play the Game:
#		- Download the needed files:
#			Game.java, Story.java, LinearStory.java, Event.java, StoryReader.java, TripStory.txt, and all corresponding images.
#		- Ensure they are within the same file with the images folder separate.
#		- Additionally, ensure that the compiled  bitcode is properly stored in their corresponding package folders. (the story folder)
#		- Compile Game.java in the command line with 'javac Game.java' 
#		- Type in 'appletviewer GameApplet.html' in order to run the applet of our game.	

# !! How to test the code:
#		- Refer to TestingInstructions.md