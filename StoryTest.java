package test;

import story.Story;
import story.LinearStory;
import story.Event;
import story.StoryReader;

/**
 *	StoryTest Class
 *	Tests Story Class
 *	
 *	@author: Sebastian Crites
 */

public class StoryTest {
	public StoryTest() {}
	
	public boolean runTest() {
		String storyTitle = "THE TITLE OF THE STORY";
		Story s = new Story(storyTitle);
		if (!storyTitle.equals(s.getTitle())) {
			System.out.println("\tTest Failed: Story_constructor");
			System.out.printf("\t\tExpected: %s\n\t\tActual: %s",storyTitle,s.getTitle());
			return storyTitle.equals(s.getTitle());
		}
		return true;
	}
}
		