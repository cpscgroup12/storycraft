package test;

import story.Story;
import story.LinearStory;
import story.Event;
import story.StoryReader;

/**
 *	StoryReaderTest Class
 *	Tests StoryReader Class
 *	
 *	@author: Sebastian Crites
 */

import java.io.File;

public class StoryReaderTest {
	public StoryReaderTest() {}
	
	public boolean runTest() {
		return constructorTest()
			& nextEventTest();
	}
	
	private boolean constructorTest() {
		String fileName = "Narration.txt";
		StoryReader sr = new StoryReader(fileName);
		File file = new File(fileName);
		if (!file.toString().equals(sr.getFile().toString())) {
			System.out.println("\tTest Failed: StoryReader_constructor");
			System.out.printf("\t\tExpected: %s\n\t\tActual: %s\n", file.toString(), sr.getFile().toString());
		}
		return file.toString().equals(sr.getFile().toString());
	}
	
	private boolean nextEventTest() {
		String fileName = "Narration.txt";
		StoryReader sr = new StoryReader(fileName);
		for (int i = 0; i < 3; i++) {
			String expectedImageURL = "none";
			String expectedTitle;
			String expectedNarration;
			if (i == 0) {
				expectedTitle = "e1";
				expectedNarration = "Test Narration 1";
			}
			else if (i == 1) {
				expectedTitle = "e2";
				expectedNarration = "Test Narration 2";
			}
			else {
				expectedTitle = "e3";
				expectedNarration = "Test Narration 3";
			}
			Event expectedEvent = new Event(expectedImageURL,expectedTitle,expectedNarration);
			Event actualEvent = sr.nextEvent();
			if (!expectedEvent.getIcon().toString().equals(actualEvent.getIcon().toString())) {
				System.out.println("\tTest Failed: StoryReader_nextEvent_imageURL #" + (i+1));
				System.out.printf("\t\tExpected: %s\n\t\tActual: %s\n",expectedEvent.getIcon().toString(),actualEvent.getIcon().toString());
				return expectedEvent.getIcon().toString().equals(actualEvent.getIcon().toString());
			}
			if (!expectedEvent.getTitle().equals(actualEvent.getTitle())) {
				System.out.println("\tTestFailed: StoryReader_nextEvent_Title #" + (i+1));
				System.out.printf("\t\tExpected: %s\n\t\tActual: %s\n",expectedEvent.getTitle(),actualEvent.getTitle());
				return expectedEvent.getTitle().equals(actualEvent.getTitle());
			}
			if (!expectedEvent.getNarration().equals(actualEvent.getNarration())) {
				System.out.println("\tTestFailed: StoryReader_nextEvent_Title #" + (i+1));
				System.out.printf("\t\tExpected: %s\n\t\tActual: %s\n",expectedEvent.getNarration(),actualEvent.getNarration());
				return expectedEvent.getNarration().equals(actualEvent.getNarration());
			}
		}
		return true;
	}
}
			
			
			
			
			
			
			
			
			
			
			
			
			
		  