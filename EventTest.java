package test;

import story.Story;
import story.LinearStory;
import story.Event;
import story.StoryReader;

/**
 *	EventTest Class
 *	Tests Event Class
 *	
 *	@author: Sebastian Crites
 */

import javax.swing.ImageIcon;

public class EventTest {
	public EventTest() {}
			
	private boolean stringTest (String testName, String expected, String actual) {
		if (!expected.equals(actual)) {
			System.out.println("\tTest Failed: " + testName);
			System.out.printf("\t\tExpected: %s\n\t\tActual: %s\n", expected, actual);
		}
		return expected.equals(actual);
	}
	
	private boolean imageIconTest (String testName, ImageIcon expected, ImageIcon actual) {
		if (expected == null && actual == null){
			return true;
		}
		if (!expected.toString().equals(actual.toString())) {
			System.out.println("\tTest Failed: " + testName);
			System.out.printf("\t\tExpected: %s\n\t\tActual: %s\n", expected.toString(), actual.toString());
		}
		return expected.toString().equals(actual.toString());
	}
	
	public boolean runTest() {
		return default_constructorTest() 
			& imageURL_isSpecified_constructorTest()
			& imageURL_isNotSpecified_constructorTest();
	}
	
	private boolean default_constructorTest() {
		Event event = new Event();
		ImageIcon expectedIcon = null;
		String expectedTitle = "";
		String expectedNarration = "<html><body style='width: 500px; text-align: justify; padding-left: 25px;'></body></html>";
		ImageIcon actualIcon = event.getIcon();
		String actualTitle = event.getTitle();
		String actualNarration = event.getNarration();
		return imageIconTest("Event_default_constructor_imageIcon", expectedIcon, actualIcon)
			& stringTest("Event_default_constructor_title", expectedTitle, actualTitle)
			& stringTest("Event_default_constructor_narration", expectedNarration, actualNarration);
	}
	
	private boolean imageURL_isSpecified_constructorTest() {
		String eventImageURL = "";
		String eventTitle = "This is the title";
		String eventNarration = "This is the Narration";
		Event event = new Event(eventImageURL, eventTitle, eventNarration);
		ImageIcon expectedIcon = new ImageIcon(eventImageURL);
		String expectedTitle = eventTitle;
		String expectedNarration = "<html><body style='width: 500px; text-align: justify; padding-left: 25px;'>This is the Narration</body></html>";
		ImageIcon actualIcon = event.getIcon();
		String actualTitle = event.getTitle();
		String actualNarration = event.getNarration();
		return imageIconTest("Event_imageURL_isSpecified_constructor_imageIcon", expectedIcon, actualIcon)
			& stringTest("Event_imageURL_isSpecified_constructor_title", expectedTitle, actualTitle)
			& stringTest("Event_imageURL_isSpecified_constructor_narration", expectedNarration, actualNarration);
	}

	private boolean imageURL_isNotSpecified_constructorTest() {
		String eventImageURL = "NOPE";
		String eventTitle = "This is the title";
		String eventNarration = "This is the Narration";
		Event event = new Event(eventImageURL, eventTitle, eventNarration);
		//ImageIcon expectedIcon = new ImageIcon("images/titlescreen.gif");
		ImageIcon expectedIcon = null;
		String expectedTitle = eventTitle;
		String expectedNarration = "<html><body style='width: 500px; text-align: justify; padding-left: 25px;'>This is the Narration</body></html>";
		ImageIcon actualIcon = event.getIcon();
		String actualTitle = event.getTitle();
		String actualNarration = event.getNarration();
		return imageIconTest("Event_imageURL_isNotSpecified_constructor_imageIcon", expectedIcon, actualIcon)
			& stringTest("Event_imageURL_isNotSpecified_constructor_title", expectedTitle, actualTitle)
			& stringTest("Event_imageURL_isNotSpecified_constructor_narration", expectedNarration, actualNarration);
	}
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		
	
		