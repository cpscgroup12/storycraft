package story;

/**
 *	Event Class
 *	Class that defines all Event Objects.
 *	
 *	@author: Rik Mantel
 */

import javax.swing.ImageIcon;

public class Event
{
	
	//Instance variables that define all of the properties of an Event.
	private ImageIcon portraitIcon; 
	private String title;
	private String narration;
	private boolean hasIcon;
	
	/**
	*	Default Constructor Method
	*	
	*	@param: none.
	*	@return: none.
	*/
	public Event()
	{
		portraitIcon = null;
		title = "";
		narration = "";
		hasIcon = false;
	}
	
	/**
	*	Custom Constructor Method for creating an event with specifics.
	*	
	*	@param: String imageURL, String eventTitle, String eventNarration
	*	@return: none.
	*/
	public Event(String imageURL, String eventTitle, String eventNarration)
	{
		if(imageURL != "NOPE")
		{
			portraitIcon = new ImageIcon(imageURL);
			hasIcon = true;
		}else if(imageURL == "NOPE")
		{
			portraitIcon = null;
			hasIcon = false;
		}else
		{
			portraitIcon = null;
			hasIcon = false;
			// Something went wrong if we ended up here.
		}
		title = eventTitle;
		narration = eventNarration; 
	}
	
	/**
	*	Accessor
	*	Returns the current value for the portraitIcon of this Event
	*	
	*	@param: none.
	*	@return: ImageIcon portraitIcon.
	*/
	public ImageIcon getIcon()
	{
		return portraitIcon;
	}
	
	/**
	*	Accessor
	*	Returns the current value for the title of this Event
	*	
	*	@param: none.
	*	@return: String title.
	*/
	public String getTitle()
	{
		return title;
	}
	
	/**
	*	Accessor
	*	Returns the current value for the narration of this Event
	*	
	*	@param: none.
	*	@return: the modified String narration.
	*/
	public String getNarration()
	{
		return "<html><body style='width: 500px; text-align: justify; padding-left: 25px;'>" + narration + "</body></html>";
	}
	
	/**
	*	Accessor
	*	Returns the current value for the icon of this Event
	*	
	*	@param: none.
	*	@return: boolean hasIcon.
	*/
	public boolean iconAvailability()
	{
		return hasIcon;
	}
	
	/**
	*	Mutator
	*	Modifies the value of the current portraitIcon.
	*	
	*	@param: String url.
	*	@return: none.
	*/
	public void setImage(String url)
	{
		this.portraitIcon = new ImageIcon(url+"");
	}
	
	/**
	*	Mutator
	*	Modifies the value of the current title.
	*	
	*	@param: String title.
	*	@return: none.
	*/
	public void setTitle(String title)
	{
		this.title = title;
	}
	
	/**
	*	Mutator
	*	Modifies the value of the current narration.
	*	
	*	@param: String narration.
	*	@return: none.
	*/
	public void setNarration(String narration)
	{
		this.narration = narration;
	}
}