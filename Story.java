package story;

/**
 *	Story Class
 *	Parent Class of all Story Types. Defines that all stories have a title.
 *	
 *	@author: Rik Mantel
 */

public class Story
{
	//	Instance Variable to define the story's title.
	private String title;
	
	/**
	*	Constructor Method for a custom title.
	*	
	*	@param: String title.
	*	@return: none.
	*/
	public Story(String title)
	{
		this.title = title;
	}
	
	/**
	*	Accessor
	*	get Method to check the title of this story.
	*	
	*	@param: none.
	*	@return: String title.
	*/
	public String getTitle()
	{
		return title;
	}
}