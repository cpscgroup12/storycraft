/**
 *	Game Class
 *	The JApplet extension. The Game Class contains all of the goodies
 *	displayed on the Applet. Also handles what content to display.
 *	
 *	@author: Rik Mantel
 */

import javax.swing.*;
import java.awt.*;
import java.io.*;

import java.applet.Applet;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.imageio.ImageIO;
import java.util.LinkedList;
import java.awt.List;

import story.Story;
import story.LinearStory;
import story.Event;
import story.StoryReader;

public class Game extends JApplet implements ActionListener
{
	private Container contentPane;
	
	//	Our Button for the player to interact with.
	private JButton nextButton;
	
	//	Some goodies for the management of the Events.
	private JLabel imagePort;
	private JLabel storyText;
	
	/*
	 *	BUILD THE STORY.
	 *		Below are the crucial points to actually bring in the document into the applet.
	 *		We've included two example stories to display, to show that our program actually works (sort of).
	 *		TripStory.txt is the default story to display. You can comment that line out and use the other one, to test.
	 */
	 
	private LinearStory story = new LinearStory("Trip Adventure", "TripStory.txt", 24);	
	//private LinearStory story = new LinearStory("Coding Crisis", "CodingCrisis.txt", 10); // Another story example.
	private LinkedList<Event> eventList = story.buildStory();
	
	/**
	*	init(): dictates what's displayed on our Applet. 
	*	
	*	@param: none.
	*	@return: none.
	*/
	public void init()
	{	
		Frame c = (Frame)this.getParent().getParent();
		c.setTitle(story.getTitle());
	
		// Set up the Container and set its background.
		contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());
		contentPane.setBackground(new Color(31,30,35));
		
		// Attempt to load our event image(s). 
		try
		{ 
			ImageIcon icon = new ImageIcon("images/titlescreen.jpg"); 
			imagePort = new JLabel("Image and Text", icon, JLabel.CENTER); 
			
			// Add the image to the container.
			contentPane.add(imagePort, BorderLayout.PAGE_START);
		}
		catch(Exception e) {
			System.out.println("There was an error with loading the image.");
		 }
		
		// The title screen to our game. Sets the design for the game text.
		storyText = new JLabel("<html><body style='width: 500px; text-align: justify; padding-left: 25px;'><font color='white'><b>StoryCraft</b></font> Pre-Pre-Alpha. <br /> Load your own adventure document and start playing! Documentation available at BitBucket: <br /><font color='#ece7ff;'><em>https://bitbucket.org/cpscgroup12/storycraft</em></font></body></html>");
		storyText.setFont(new Font("Tahoma", Font.PLAIN, 14));
		storyText.setForeground(new Color(163,163,165));
		
		// Add the text to the container.
		contentPane.add(storyText, BorderLayout.CENTER);
		
		// Setting up the continue button and giving it a custom look.
		nextButton = new JButton();
		nextButton.setBorderPainted(false);
 		nextButton.setBorder(null);
	 	nextButton.setFocusable(false);
		nextButton.setMargin(new Insets(0, 5, 0, 0));
	 	nextButton.setContentAreaFilled(false);
	 	nextButton.setIcon(new ImageIcon("images/next.jpg"));
	 	nextButton.setRolloverIcon(new ImageIcon("images/nextHover.jpg"));
	 	nextButton.setPressedIcon(new ImageIcon("images/next.jpg"));
	 	nextButton.setDisabledIcon(new ImageIcon("images/nextDisabled.jpg"));
		nextButton.addActionListener(this);
		
		//Add the continue button to our container.
		contentPane.add(nextButton, BorderLayout.LINE_END);	
	}
	
	/**
	*	actionPerformed(ActionEvent e): dictates what code is executed when an item on the 
	*	applet is pressed. Good for all sorts of things.
	*	
	*	@param: none.
	*	@return: none.
	*/
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == nextButton)
		{
			if(!eventList.isEmpty())
			{
				Event e1 = eventList.pop();
				storyText.setText(e1.getNarration());
				// imagePort.setIcon(e1.getIcon()); // Feature not implemented I'm Sorry Leanne.
			}else
			{
				storyText.setText("<html><body style='width: 500px; text-align: justify; padding-left: 25px;'>- - - <em>Finished</em><br />Game displayed using <b>StoryCraft</b>.</body></html>");
				nextButton.setEnabled(false);
			}
			
		}
	}
}