package test;

import story.Story;
import story.LinearStory;
import story.Event;
import story.StoryReader;

/**
 *	MasterTest Class
 *	Runs all the tests for each class
 *	
 *	@author: Sebastian Crites
 */

public class MasterTest {
	public MasterTest() {}
	
	public void run() {
		System.out.println("TESTING CLASSES...");
		StoryTest st = new StoryTest();
		EventTest et = new EventTest();
		StoryReaderTest srt = new StoryReaderTest();
		LinearStoryTest lst = new LinearStoryTest();
		boolean testsPassed = true;
		testsPassed = testsPassed & st.runTest();
		testsPassed = testsPassed & et.runTest();
		testsPassed = testsPassed & srt.runTest();
		testsPassed = testsPassed & lst.runTest();
		if (testsPassed) {
			System.out.println("\tAll tests were completed and successful! =D");
		}
	}
		
	public static void main(String[] args) {
		MasterTest mt = new MasterTest();
		mt.run();
	}
}
		
		