package test;

import story.Story;
import story.LinearStory;
import story.Event;
import story.StoryReader;

/**
 *	LinearStoryTest Class
 *	Tests LinearStory Class
 *	
 *	@author: Sebastian Crites
 */

import java.util.LinkedList;

public class LinearStoryTest {
	public LinearStoryTest() {}
	
	public boolean runTest() {
		return constructorTest()
			& buildStoryTest();
	}
	
	private boolean constructorTest() {
		String linearStoryTitle = "THIS IS THE TITLE";
		String linearStoryFileName = "Narration.txt";
		int linearStoryLength = 3;
		LinearStory s = new LinearStory(linearStoryTitle, linearStoryFileName, linearStoryLength);
		if (linearStoryTitle != s.getTitle()) {
			System.out.println("\tTest Failed: linearStory_constructor_Title");
			System.out.printf("\t\tExpected: %s\n\t\tActual: %s",linearStoryTitle,s.getTitle());
			return linearStoryTitle == s.getTitle();
		}
		StoryReader sr = new StoryReader(linearStoryFileName);
		if (!sr.getFile().toString().equals(s.getStory().getFile().toString())) {
			System.out.println("\tTestFailed: linearStory_constructor_Story");
			System.out.printf("\t\tExpected: %s,\n\t\tActual: %s\n",sr.getFile().toString(),s.getStory().getFile().toString());
			return sr.getFile().toString().equals(s.getStory().getFile().toString());
		}
		if (linearStoryLength != s.length()) {
			System.out.println("\tTestFailed: linearStory_constructor_StoryLength");
			System.out.println("\t\tExpected: " + linearStoryLength + "\n\t\tActual: " + s.length());
			return linearStoryLength == s.length();
		}
	return true;
	}
	
	private boolean buildStoryTest() {
		String linearStoryTitle = "THIS IS THE TITLE";
		String linearStoryFileName = "Narration.txt";
		int linearStoryLength = 3;
		LinearStory s = new LinearStory(linearStoryTitle, linearStoryFileName, linearStoryLength);
		LinkedList<Event> actualEvents = s.buildStory();
		LinkedList<Event> expectedEvents = new LinkedList<Event>();
		for (int i = 0; i < 3; i++) {
			String expectedImageURL = "none";
			String expectedTitle;
			String expectedNarration;
			if (i == 0) {
				expectedTitle = "e1";
				expectedNarration = "Test Narration 1";
			}
			else if (i == 1) {
				expectedTitle = "e2";
				expectedNarration = "Test Narration 2";
			}
			else {
				expectedTitle = "e3";
				expectedNarration = "Test Narration 3";
			}
			Event e = new Event(expectedImageURL,expectedTitle,expectedNarration);	
			expectedEvents.addLast(e);
		}
		for (int j = 0; j < expectedEvents.size(); j++) {
			if (!(actualEvents.get(j).getIcon().toString().equals(expectedEvents.get(j).getIcon().toString())) 
				|| !(actualEvents.get(j).getTitle().equals(expectedEvents.get(j).getTitle()))
				|| !(actualEvents.get(j).getNarration().equals(expectedEvents.get(j).getNarration()))) {
				System.out.println("\tTestFailed: linearStory_buildStory #" + j);
				System.out.println("\t\tExpected: " + expectedEvents.get(j).getIcon().toString() +
									", " + expectedEvents.get(j).getTitle() +
									", " + expectedEvents.get(j).getNarration() +
									"\n\t\tActual: " + actualEvents.get(j).getIcon().toString() +
									", " + actualEvents.get(j).getTitle() +
									", " + actualEvents.get(j).getNarration());
				return (actualEvents.get(j).getIcon().toString().equals(expectedEvents.get(j).getIcon().toString())) 
					& (actualEvents.get(j).getTitle().equals(expectedEvents.get(j).getTitle()))
					& (actualEvents.get(j).getNarration().equals(expectedEvents.get(j).getNarration()));
			}
		}
		return true; 
	}		
}
			
			
	