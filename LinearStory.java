package story;

/**
 *	LinearStory Class
 *	Child class of Story which specifies a linear based story for the game.
 *	
 *	@author: Rik Mantel, Sebastian Crites
 */

import java.util.LinkedList;

public class LinearStory extends Story
{
	//	Basic properties of a Linear Story, which includes a list of events, a length, and story reader to build the story.
	private LinkedList<Event> eventList;
	private StoryReader story;
	private int storyLength;
	
	/**
	*	Constructor Method for building a customized LinearStory
	*	
	*	@param: String title, String filename, int storyLength
	*	@return: none.
	*/
	public LinearStory(String title, String filename, int storyLength)
	{
		super(title);
		story = new StoryReader(filename);
		this.storyLength = storyLength;
	}
	
	/**
	*	Mutator
	*	buildStory creates this actual LinearStory to be used by the game and loads the linked list.
	*	
	*	@param: none.
	*	@return: LinkedList<Event> eventList.
	*/
	public LinkedList<Event> buildStory()
	{
		eventList = new LinkedList<Event>();
		for(int x = 0; x < storyLength; x++)
		{
			eventList.add(story.nextEvent());
		}
		
		return eventList;
		
	}
	
	/**
	*	Accessor
	*	Returns the current value for the story of this LinearStory
	*	
	*	@param: none.
	*	@return: StoryReader story.
	*/
	public StoryReader getStory() 
	{
		return story;
	}
	
	/**
	*	Accessor
	*	Returns the current value for the storyLength of this LinearStory
	*	
	*	@param: none.
	*	@return: int storyLength.
	*/
	public int length() 
	{
		return storyLength;
	}
}