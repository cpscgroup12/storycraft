package story;

/**
 *	StoryReader
 *	A class that creates an object to read in story details from a file.
 *	
 *	@author: Rik Mantel, Richard Pham, Sebastian Crites
 */

import java.io.*;
import java.util.Scanner;
import java.util.LinkedList;
import java.io.File;
import java.io.FileNotFoundException;

public class StoryReader
{
	//	Instance variables of a StoryReader, which includes a scanner and a file.
	private Scanner scanner;
	private File file;

	/**
	*	Constructor Method the story reader to read in a file.
	*	
	*	@param: String fileName.
	*	@return: none.
	*/
	public StoryReader(String fileName)
	{
		file = new File(fileName);
		try
		{
			scanner = new Scanner(file);
		}
		catch(FileNotFoundException e)
		{
			System.err.println("FileNotFoundException: " + e.getMessage());
		}
		catch(IOException e)
		{
			System.err.println("IOException: " + e.getMessage());
		}
	}
	
	/**
	*	Accessor
	*	nextEvent is the magic method that will take in the next event's details from the Story Document and return it as an Event Object.
	*	
	*	@param: none.
	*	@return: Event e.
	*/
	public Event nextEvent()
	{
		String imageUrl = scanner.nextLine();
		String title = scanner.nextLine();
		String narration = scanner.nextLine();
		Event e = new Event(imageUrl, title, narration);
		
		return e;
	}
	
	/**
	*	Accessor
	*	Returns the current file of this StoryReader
	*	
	*	@param: none.
	*	@return: File file.
	*/
	
	public File getFile() 
	{
		return file;
	}
	
}